FROM ekidd/rust-musl-builder as builder
USER rust
WORKDIR /home/rust/src/
COPY . .
RUN sudo chown -R rust *
RUN cargo build --target armv7-unknown-linux-musleabihf --release
RUN cargo install --target armv7-unknown-linux-musleabihf --path .

FROM arm32v7/alpine:latest
COPY --from=builder /home/rust/.cargo/bin/k3s_hello_world /usr/local/bin/k3s_hello_world
ENV RUST_LOG=warn
CMD ["k3s_hello_world"]
