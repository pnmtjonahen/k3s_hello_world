#!/bin/bash

#cargo build --target=armv7-unknown-linux-gnueabihf --release

docker build . -t pnmtjonahen/k3s_hello_world
docker push pnmtjonahen/k3s_hello_world:latest

kubectl delete -f k3s_hello_world.yml
kubectl apply -f k3s_hello_world.yml
kubectl get pods -o wide