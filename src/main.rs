use futures::future;
use hyper::rt::Future;
use hyper::service::service_fn;
use hyper::{Body, Request, Response, Server};


type BoxFuture = Box<dyn Future<Item = Response<Body>, Error = hyper::Error> + Send>;

fn handler_service(_req: Request<Body>) -> BoxFuture {
    Box::new(future::ok(Response::new(Body::from("Hello, World!"))))
}

fn main() {
    let addr = ([0, 0, 0, 0], 3000).into();

    let server = Server::bind(&addr)
        .serve(|| service_fn(handler_service))
        .map_err(|e| eprintln!("server error: {}", e));

    hyper::rt::run(server);
}